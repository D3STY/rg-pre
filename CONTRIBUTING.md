# Contributing to rg-pre

Thank you for considering contributing to rg-pre! By contributing, you help make this project better for everyone. We appreciate your time and effort.

## Code of Conduct

Please note that this project is released with a Contributor Code of Conduct. By participating in this project, you agree to abide by its terms. Please review the [Code of Conduct](CODE_OF_CONDUCT.md) before contributing.

## How to Contribute

1. Fork the repository to your GitHub account.
2. Clone the forked repository to your local machine:
3. Create a new branch for your feature or bug fix:
4. Make your changes and commit them with a clear and concise commit message:
5. Push your changes to your GitHub repository:
6. Open a pull request on the `main` branch of the original repository.

## Code Guidelines

- Follow the coding style and conventions used in the project.
- Write clear, concise, and well-documented code.
- Test your changes thoroughly before submitting a pull request.
- Ensure your code does not introduce new issues.

## Bug Reports

If you find a bug, please open an issue and provide the following information:
- A clear and descriptive title.
- Steps to reproduce the bug.
- Expected behavior and actual behavior.
- Any relevant error messages or screenshots.

## Feature Requests

If you have a feature request, open an issue and provide:
- A clear and detailed description of the feature.
- Any relevant context or examples.

## Pull Request Checklist

Before submitting a pull request, please ensure that:
- Your code follows the project's coding standards.
- You have added appropriate tests for your changes.
- The documentation is updated to reflect your changes.

## Licensing

By contributing, you agree that your contributions will be licensed under the project's [LICENSE](LICENSE) file.

Thank you for your contributions!

